setTimeout(() => {
    fetch("/script/data.yml")
    .then(response => response.text().then(data => {
        setGifs(YAML.parse(data));
        lazyLoading();
    }))
}, 150);

const updateTags = () => {
    let tagsToShow = [];
    let tagsToNotShow = [];

    document.querySelectorAll('.form-check-input').forEach(element => 
        (element.checked ? tagsToShow : tagsToNotShow).push(element.value)
    );

    if (tagsToShow.length == 0) tagsToShow = tagsToNotShow;

    document.querySelectorAll('.card').forEach(element => {
        element.style.display = (
            tagsToShow.some(tag => element.classList.contains(`tag-${tag}`)) &&
            (element.classList.contains('is-nsfw') ? document.getElementById("nsfwCheckbox").checked : !document.getElementById("nsfwCheckbox").checked)
        ) ? 'unset' : 'none';
    })
}

const setGifs = gifs => {
    const gifsElement = document.getElementById("gifs");
    const tagsElement = document.getElementById("normal-tags");
    const tags = [];

    gifs.forEach(gif => {
        // Construct Div
        let div = document.createElement("div");
        div.classList.add('card');
        gif.tags.forEach(tag => {
            tags.push(tag);
            div.classList.add(`tag-${tag}`); // TODO: Make work for tags with spacings
        });
        if (gif.nsfw) div.classList.add('is-nsfw');
        div.style.display = 'none';

        // Construct Image
        let img = document.createElement("img");
        if (/^(http|https):\/\/[^ "]+$/.test(gif.url)) {
            img.dataset.src = gif.url;
            img.onclick = () => copyToMe(gif.url);
        } else {
            img.dataset.src = `${window.location.origin}/src/${gif.url}-proxy.webp`;
            img.onclick = () => copyToMe(`${window.location.origin}/src/${gif.url}`);
            img.onerror = () => img.src = `${window.location.origin}/src/${gif.url}`;
        }
        img.className = "card-img-top";

        div.appendChild(img);
        gifsElement.appendChild(div);
    });

    var counts = {};
    tags.forEach(e => counts[e] = 1 + (counts[e] || 0) );
    
    Object.entries(counts).sort( ([,a], [,b]) => a - b).forEach(([tag, count]) =>
        tagsElement.insertAdjacentHTML('afterend', `
        <input class="form-check-input" type="checkbox" id="inlineCheckbox-${tag}" value="${tag}" onchange="updateTags()">
        <label class="form-check-label" for="inlineCheckbox-${tag}">${tag}</label>
    `));

    updateTags();
}

//#region Utils

const myToast = Toastify({
    text: "copied to clipboard",
    duration: 2000
});

const copyToMe = text => {
    let element = document.createElement('textarea');
    element.value = text;
    document.body.appendChild(element);
    element.select();
    document.execCommand('copy');
    document.body.removeChild(element);
    myToast.showToast();
}

//#endregion

//#region Lazy Loading

function lazyLoading() {
    const images = document.querySelectorAll("[data-src]");

    images.forEach(image => {
        imgObserver.observe(image);
    })
}

function preloadImage(img) {
    const src = img.getAttribute("data-src");
    if (!src) return;

    img.src = src;
}

const imgOptions = {
    threshold: 1,
    rootMargin: "0px 0px 500px 0px"
};

const imgObserver = new IntersectionObserver((entries, imgObserver) => {
    entries.forEach(entry => {
        if(!entry.isIntersecting) return;
        else {
            preloadImage(entry.target);
            imgObserver.unobserve(entry.target);
        }
    })
}, imgOptions);

//#endregion